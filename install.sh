#!/bin/sh
#
# Pre-commit hook for running checkstyle and PMD analysis on git staged files.
#
# Colors
GREEN='\033[0;32m'

cd
userDir=$(pwd)

if [ -d "$userDir/.cs-pmd/src/bin" ]; then
    # Checking for update
    echo "Checking for update" >&2
else
    # Installing for the first time
    echo $GREEN"Installing CS-PMD plugin (Static code analyser)"
    echo "Downloading binaries."
    mkdir -p $userDir/.cs-pmd
    cd $userDir/.cs-pmd
    rm -rf $userDir/.cs-pmd/src
    unset GIT_WORK_TREE
    echo "This is the one time operation and may take few minutes. Please wait..."
    git clone --depth=1 --progress --single-branch -b Develop repo-url ./src  2>&1
    echo "CS_PMD tool Successfully installed."
    cd $userDir/.cs-pmd/src
    chmod -R 777 *
fi
